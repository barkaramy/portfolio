import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Footer from '../src/components/Footer/Footer';
import HomePage from '../src/pages/HomePage';
import AboutPage from '../src/pages/AboutPage';
import ContactPage from '../src/pages/ContactPage';

class App extends React.Component {
  state = {
    title: 'Ramy BARKA',
    headerLinks: [
      {
        title: 'home',
        path: '/'
      },
      { title: 'about', path: '/about' },
      { title: 'contant', path: '/contact' }
    ],
    home: {
      title: 'Être implacable',
      subTitle: 'Des projets qui font la difference',
      text: 'Consultez mes projets ci-dessous'
    },
    about: {
      title: 'A propos de moi'
    },
    contact: {
      title: 'Parlons'
    }
  };
  render() {
    return (
      <Router>
        <Container className='p-0' fluid={true}>
          <Navbar className='border-bottom' bg='transparent' expand='lg'>
            <Navbar.Brand>Ramy BARKA</Navbar.Brand>
            <Navbar.Toggle className='border-0' aria-controls='navbar-toggle' />
            <Navbar.Collapse id='navbar-toggle'>
              <Nav className='ml-auto'>
                <Link className='nav-link' to={'/'}>
                  Home
                </Link>
                <Link className='nav-link' to={'/about'}>
                  About
                </Link>
                <Link className='nav-link' to={'/contact'}>
                  Contact
                </Link>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          <Route
            path='/'
            exact
            render={() => (
              <HomePage
                title={this.state.home.title}
                subTitle={this.state.home.subTitle}
                text={this.state.home.text}
              />
            )}
          />
          <Route
            path='/about'
            exact
            render={() => <AboutPage title={this.state.about.title} />}
          />
          <Route
            path='/contact'
            exact
            render={() => <ContactPage title={this.state.contact.title} />}
          />

          <Footer />
        </Container>
      </Router>
    );
  }
}

export default App;
