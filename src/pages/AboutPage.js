import React from 'react';
import Hero from '../components/Hero';
import Content from '../components/Content/Content';

const AboutPage = (props) => {
  return (
    <div>
      <Hero title={props.title} />
      <Content>
        <p>
          Bonjour, je m'appelle Ramy. Je suis etudiant en développement web à
          Ingésup, actuellement a la recherche d'une alternance, j'ai de
          l'expérience dans React JS, Express JS, Node JS, Oracle SQL et
          MongoDB.
        </p>
        <p>
          J'apprends constamment de nouvelles choses. actuellement, ces choses
          incluent acquérir plus d'expérience avec MongoDB, React, Express JS et
          Node JS
        </p>
        <p>
          Mon dernier projet, WeDev, est un site de gestion de projets pour les
          développeurs. Il est construit avec React JS, MongoDB, Express JS et
          Node JS.
        </p>
        <p>
          Quand je n'apprends pas quelque chose de nouveau, je lis des mangas ou
          je joue a league of legends.
        </p>
      </Content>
    </div>
  );
};

export default AboutPage;
