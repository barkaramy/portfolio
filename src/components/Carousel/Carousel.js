import React, { Component } from 'react';
import { Card } from '../Card/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import movies from '../../assets/images/movies.jpg';
import burger from '../../assets/images/burger.jpg';
import epicerie from '../../assets/images/epicerie.jpg';

class Carousel extends Component {
  state = {
    items: [
      {
        id: 0,
        title: 'Burger builder',
        subTitle: 'Construis ton burger en ligne',
        imgSrc: burger,
        selected: false
      },
      {
        id: 1,
        title: 'Épiceire',
        subTitle: 'Épicierie interactive en ligne',
        imgSrc: epicerie,
        selected: false
      },
      {
        id: 2,
        title: 'Movies',
        subTitle: 'Regardez vos films et séries préféres oû que vous soyez',
        imgSrc: movies,
        selected: false
      }
    ]
  };

  handleCardClick = (id, card) => {
    let items = [...this.state.items];
    items[id].selected = items[id].selected ? false : true;
    items.forEach((item) => {
      if (item.id !== id) {
        item.selected = false;
      }
    });
    this.setState({ items });
  };
  makeItems = (items) => {
    return items.map((item) => {
      return (
        <Card
          item={item}
          click={(e) => this.handleCardClick(item.id, e)}
          key={item.id}
        />
      );
    });
  };

  render() {
    return (
      <Container fluid={true}>
        <Row className='justify-content-around'>
          {this.makeItems(this.state.items)}
        </Row>
      </Container>
    );
  }
}

export default Carousel;
